<?php

namespace App\Wrappers\Captcha\Providers;

use App\Wrappers\Captcha\CaptchaInterface;
use Illuminate\Support\Facades\Http;

class HCaptcha implements CaptchaInterface
{
    protected $secretKey = '';
    protected $siteKey = '';

    public function verify($response): bool
    {
        $result = Http::post('https://hcaptcha.com/siteverify', [
            'secret' => $this->secretKey,
            'sitekey' => $this->siteKey,
            'response' => $response,
        ]);

        if($result['success'] === true) {
            return true;
        }

        return false;
    }

    public function getCaptchaSettings(): array
    {
        return [
            'type' => 'hcaptcha',
            'sitekey' => $this->siteKey
        ];
    }
}
