<?php

namespace App\Wrappers\Captcha;

interface CaptchaInterface
{
    public function verify($response);

    public function getCaptchaSettings();
}
