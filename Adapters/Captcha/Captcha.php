<?php

namespace App\Wrappers\Captcha;

use App\Wrappers\Captcha\Providers\HCaptcha;

class Captcha implements CaptchaInterface
{
    protected array $providers = [
        'hcaptcha' => HCaptcha::class,
    ];

    private mixed $provider;

    public function __construct($provider)
    {
        if (!array_key_exists($provider, $this->providers)) {
            throw new \Exception('Invalid provider.');
        }

        $this->provider = new $this->providers[$provider]();
    }

    public function verify($response)
    {
        return $this->provider->verify($response);
    }

    public function getCaptchaSettings()
    {
        return $this->provider->getCaptchaSettings();
    }
}
